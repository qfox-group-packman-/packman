import pygame
import sys
from menu import Menu, LifeIcon
from packman_class import Packman
from map import Map
from ghost_class import Ghost, Fire
from functions import reset_all, set_difficulty, check_score, draw_all, cheats
import time
import random

size = width, height = 500, 600
black = 0, 0, 0

def main():
    pygame.init()
    screen = pygame.display.set_mode(size)
    gameover = False
    map = Map()
    menu = Menu()
    pacman = Packman()
    counter = 0 # таймер отвечающий за закрытие стартовой комнаты призраков
    invisiblecounter = 8 # таймер отвечающий за время действия бустеров
    ghosts = [] # массив призраков
    ghosts.append(Ghost("red", 250, 236))
    ghosts.append(Ghost("pink", 250, 283))
    ghosts.append(Ghost("blue", 218, 283))
    ghosts.append(Ghost("orange", 282, 283))
    emptyroom = False # ограничивает закрытие стартовой комнаты лишь одной стеной
    lifeicons = [LifeIcon(39, 573), LifeIcon(86, 573)] # массив иконок
    pause = False
    clock = pygame.time.Clock()
    FPS = 60 # ограничение кадров

    while not gameover:
        if (counter < 2 and pause == False):
             counter += 0.01
        if (invisiblecounter < 5 and pause == False and menu.cheats == False):
             invisiblecounter += 0.01
        clock.tick(FPS)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                gameover = True

        screen.fill(black)
        if (menu.mode != "play"):
            pressed = pygame.mouse.get_pressed()
            pos = pygame.mouse.get_pos()
            if pressed[0]:
                menu.CheckButtons(pos[0], pos[1])
            menu.Update(screen)
            FPS = menu.fps

        if (menu.mode == "play" and pause == False):
            if (menu.start):
                # если перезагрузка вызвана нажатие start в меню
                reset_all(ghosts, pacman, emptyroom, map, size)
                menu.level = 1
                invisiblecounter = 8
                counter = 0
                emptyroom = False
                pacman.score = 0
                map.__init__()
                menu.start = False
                lifeicons.clear()
                lifeicons = [LifeIcon(39, 573), LifeIcon(86, 573)]
                set_difficulty(menu, ghosts, pacman)
            invisiblecounter = pacman.pacman_movement(event, ghosts, menu, map, invisiblecounter) #движение пакмэна
            set_difficulty(menu, ghosts, pacman)
            #######################
            # движение призраков и пакмэна
            for i in range(len(ghosts)):
                ghosts[i].ghosts_movement(pacman, map) #движение призраков

            if (counter >= 1.4 / pacman.maxspeed and emptyroom == False): # закрываем комнату созданием новой стены
                # считаем по таймеру
                map.walls.append(pygame.Rect((194, 252), (111, 63)))
                emptyroom = True
            #######################
            check_score(pacman, ghosts)

            if (pacman.ghost_collision(ghosts)):
                # При столкновении пакмэна и привидений сбрасываем их параметры и убавляем кол-во жизней
                pacman.lifes -= 1
                pacman.death_animation(screen)
                if (pacman.lifes <= -1 and menu.mode == "play"):
                    counter = 0
                    emptyroom = False
                    menu.score = pacman.score
                    menu.mode = "game over"
                    menu.start = False
                    menu.result = "You lost"
                    reset_all(ghosts, pacman, emptyroom, map, size)
                    invisiblecounter = 8
                else:
                    newscore = pacman.score
                    newlifes = pacman.lifes
                    reset_all(ghosts, pacman, emptyroom, map, size)
                    invisiblecounter = 8
                    pacman.lifes = newlifes
                    pacman.score = newscore
                    counter = 0
                    emptyroom = False
                    if (len(lifeicons) > 0):
                        del lifeicons[len(lifeicons) - 1]
                set_difficulty(menu, ghosts, pacman)

        if (len(map.grains) == 0 and menu.mode == "play" and menu.level == 2):
            # если на карте нет зерён - выход
            menu.score = pacman.score
            map.__init__()
            counter = 0
            emptyroom = False
            menu.mode = "game over"
            menu.start = False
            menu.result = "You win"
            reset_all(ghosts, pacman, emptyroom, map, size)
            invisiblecounter = 8
        elif (len(map.grains) == 0 and menu.mode == "play" and menu.level == 1):
            # если на карте нет зерён - следующий уровень
            newscore = pacman.score
            map.__init__()
            counter = 0
            emptyroom = False
            menu.start = False
            reset_all(ghosts, pacman, emptyroom, map, size)
            invisiblecounter = 8
            lifeicons.clear()
            lifeicons = [LifeIcon(39, 573), LifeIcon(86, 573)]
            menu.level = 2
            pacman.score = newscore

        if event.type == pygame.KEYDOWN:
        # пауза
            if event.key == pygame.K_p:
                if (pause == False):
                    pause = True
                else:
                    pause = False
                time.sleep(0.5)

        draw_all(screen, menu, map, pacman, ghosts, lifeicons, clock, pause)

        cheats(event, menu, map, pause, time)

        pygame.display.update()
    sys.exit()

main()