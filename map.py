import pygame
import sys
from menu import Menu
from packman_class import Packman
import random

size = width, height = 500, 600
black = 0, 0, 0

class Grains():
    def __init__(self, x, y, type="simple"): # type simple or booster
        self.type = type
        self.score = 10
        self.grain_img = pygame.image.load('resources/map/grain.png')
        self.grain_img = pygame.transform.scale(self.grain_img, (11, 11))
        if (type == "booster"):
            self.score = 50
            self.grain_img = pygame.image.load('resources/map/booster.png')
            self.grain_img = pygame.transform.scale(self.grain_img, (15, 15))
        self.grain_rect = self.grain_img.get_rect(center=(x, y))

class Map:
    def __init__(self):
        self.map_img = pygame.image.load('resources/map/map.png')
        self.map_img = pygame.transform.scale(self.map_img, (448, 496))
        self.map_rect = self.map_img.get_rect(center=(width // 2, height // 2))
        self.walls = [] # массив прямоугольников, для обработки коллизии
        self.grains = [] #массив зерен
        self.walls.append(pygame.Rect((26, 51), (8, 496)))
        self.walls.append(pygame.Rect((466, 51), (8, 496)))

        self.walls.append(pygame.Rect((26, 51), (440, 8)))
        self.walls.append(pygame.Rect((26, 540), (440, 8)))

        self.walls.append(pygame.Rect((26, 204), (87, 63)))
        self.walls.append(pygame.Rect((26, 300), (87, 63)))
        self.walls.append(pygame.Rect((386, 204), (88, 63)))
        self.walls.append(pygame.Rect((386, 300), (88, 63)))

        self.walls.append(pygame.Rect((66, 92), (47, 31)))
        self.walls.append(pygame.Rect((386, 92), (47, 31)))

        self.walls.append(pygame.Rect((146, 92), (63, 31)))
        self.walls.append(pygame.Rect((290, 92), (63, 31)))

        self.walls.append(pygame.Rect((66, 156), (47, 15)))
        self.walls.append(pygame.Rect((386, 156), (47, 15)))

        self.walls.append(pygame.Rect((146, 156), (15, 111)))
        self.walls.append(pygame.Rect((338, 156), (15, 111)))

        self.walls.append(pygame.Rect((146, 300), (15, 63)))
        self.walls.append(pygame.Rect((338, 300), (15, 63)))

        self.walls.append(pygame.Rect((146, 444), (15, 63)))
        self.walls.append(pygame.Rect((338, 444), (15, 63)))

        self.walls.append(pygame.Rect((66, 492), (143, 15)))
        self.walls.append(pygame.Rect((290, 492), (143, 15)))

        self.walls.append(pygame.Rect((146, 396), (63, 15)))
        self.walls.append(pygame.Rect((290, 396), (63, 15)))

        self.walls.append(pygame.Rect((194, 444), (111, 15)))
        self.walls.append(pygame.Rect((194, 348), (111, 15)))
        self.walls.append(pygame.Rect((194, 156), (111, 15)))

        self.walls.append(pygame.Rect((242, 58), (15, 65)))
        self.walls.append(pygame.Rect((242, 156), (15, 63)))
        self.walls.append(pygame.Rect((242, 348), (15, 63)))
        self.walls.append(pygame.Rect((242, 444), (15, 63)))

        self.walls.append(pygame.Rect((146, 204), (63, 15)))
        self.walls.append(pygame.Rect((290, 204), (63, 15)))

        self.walls.append(pygame.Rect((66, 396), (47, 15)))
        self.walls.append(pygame.Rect((386, 396), (47, 15)))

        self.walls.append(pygame.Rect((98, 396), (15, 63)))
        self.walls.append(pygame.Rect((386, 396), (15, 63)))

        self.walls.append(pygame.Rect((26, 444), (39, 15)))
        self.walls.append(pygame.Rect((434, 444), (39, 15)))

        self.walls.append(pygame.Rect((194, 308), (111, 7)))
        self.walls.append(pygame.Rect((194, 252), (39, 7)))
        self.walls.append(pygame.Rect((266, 252), (39, 7)))
        self.walls.append(pygame.Rect((194, 252), (7, 63)))
        self.walls.append(pygame.Rect((298, 252), (7, 63)))

        # расстявляем точки везде, а затем удаляем ненужные"""
        for i in range(29):
            for j in range(26):
                self.grains.append(Grains(48 + j * 16, 75 + i * 16))
        delelements = [] #удаляемые точки
        additionalrect = pygame.Rect(((144, 200), (212, 164))) #прямоугольник для клетки привидений
        for i in range(len(self.walls)):
            for j in range(len(self.grains)):
                if (self.walls[i].colliderect(self.grains[j].grain_rect) == True
                    or self.grains[j].grain_rect.colliderect(additionalrect) == True) \
                    and (j in delelements) == False:
                    delelements.append(j)
        delelements.sort(reverse=True)
        for i in range(len(delelements)):
            del self.grains[delelements[i]]

        # добавляем бустеры
        self.grains[30].__init__(self.grains[30].grain_rect.centerx,
                                 self.grains[30].grain_rect.centery, "booster")
        self.grains[35].__init__(self.grains[35].grain_rect.centerx,
                                 self.grains[35].grain_rect.centery, "booster")
        self.grains[168].__init__(self.grains[168].grain_rect.centerx,
                                 self.grains[168].grain_rect.centery, "booster")
        self.grains[189].__init__(self.grains[189].grain_rect.centerx,
                                  self.grains[189].grain_rect.centery, "booster")

    def update(self, screen):
        """Отрисовка карты"""
        screen.blit(self.map_img, self.map_rect)
        for i in range(len(self.grains)):
            screen.blit(self.grains[i].grain_img, self.grains[i].grain_rect)
