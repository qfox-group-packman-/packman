import pygame
from game_object import GameObject
import time

width, height = 500, 600

black = (0, 0, 0)

class Packman(GameObject):
    def __init__(self):
        super().__init__()
        self.score = 0
        self.lifes = 2
        self.coordinates = [0, 0]
        self.speed = [0, 0]  # Скорость движения пакмана по осям OX и OY
        self.pack_man = pygame.image.load("resources/textures/pacman.png").convert_alpha()
        self.pack_man = pygame.transform.scale(self.pack_man, (32, 32))
        self.pack_man_rect = pygame.Rect(0, 0, 32, 32)
        self.pack_man_rect.center = (width // 2, height // 2 + 32)
        # Представление пакмана в виде прямоугольника
        self.invincible = False  # Режим пакмана, когда привидения не могут его съесть
        self.visible = True  # Видимость пакмана
        self.newspeed = [0, 0]
        self.counter = 0
        self.caughtghost = 0
        self.maxspeed = 2 # устанавливает скорость пакмэна

    def set_pos(self):
        """Устанавливает позицию отрисовки пакмена."""
        pass

    def shift(self, width, height):
        """Метод который обрабатывает движение пакмана."""
        self.counter += 0.25
        if (self.counter >= 2):
            self.counter = 0
        self.update_texture()
        self.pack_man_rect = self.pack_man_rect.move(self.speed)

    def update_texture(self):
        """Метод отвечающий за анимацию пакмэна"""
        pos = [self.pack_man_rect.centerx, self.pack_man_rect.centery]
        if (0 <= self.counter <= 1):
            self.pack_man = pygame.image.load("resources/textures/pacman.png").convert_alpha()
        else:
            self.pack_man = pygame.image.load("resources/textures/pacman1.png").convert_alpha()
        if (self.speed[0] < 0):
            self.pack_man = pygame.transform.flip(self.pack_man, True, False)
        elif (self.speed[1] < 0):
            self.pack_man = pygame.transform.rotate(self.pack_man, 90)
        elif (self.speed[1] > 0):
            self.pack_man = pygame.transform.rotate(self.pack_man, -90)
        self.pack_man = pygame.transform.scale(self.pack_man, (32, 32))
        self.pack_man_rect = pygame.Rect(0, 0, 32, 32)
        self.pack_man_rect.center = (pos[0], pos[1])

    def death_animation(self, screen):
        """Метод который проигрывает анимацию смерти пакмэна"""
        pygame.mixer.music.load('resources/sounds/pacman_death.wav')
        pygame.mixer.music.set_volume(0.2)
        pygame.mixer.music.play()
        pos = [self.pack_man_rect.centerx, self.pack_man_rect.centery]
        for i in range(6):
             self.pack_man = pygame.image.load("resources/textures/death" + str(i) + ".png").convert_alpha()
             self.pack_man = pygame.transform.scale(self.pack_man, (32, 32))
             self.pack_man_rect = pygame.Rect(0, 0, 32, 32)
             self.pack_man_rect.center = (pos[0], pos[1] - 4)
             screen.fill(black)
             screen.blit(self.pack_man, self.pack_man_rect)
             pygame.display.update()
             time.sleep(0.25)

    def process_event(self, event, walls, controltype):
        """Метод который обрабатывает управление пакманом (клавишами - WASD/стрелки)"""
        if event.type == pygame.KEYDOWN:
            if ((event.key == pygame.K_a and controltype == "keys") or
                (event.key == pygame.K_LEFT  and controltype == "arrows")) and self.speed[0] == 0:
                self.newspeed[0] = -self.maxspeed
            elif ((event.key == pygame.K_d and controltype == "keys") or
                (event.key == pygame.K_RIGHT  and controltype == "arrows")) and self.speed[0] == 0:
                self.newspeed[0] = self.maxspeed
            elif ((event.key == pygame.K_w and controltype == "keys") or
                (event.key == pygame.K_UP  and controltype == "arrows")) and self.speed[1] == 0:
                self.newspeed[1] = -self.maxspeed
            elif ((event.key == pygame.K_s and controltype == "keys") or
                (event.key == pygame.K_DOWN  and controltype == "arrows")) and self.speed[1] == 0:
                self.newspeed[1] = self.maxspeed
        if ((self.newspeed[0] == 0 and self.newspeed[1] == 0) \
                or (self.newspeed[0] != 0 and self.newspeed[1] != 0)):
            return

        # заранее просчитываем коллизию
        for i in range(len(walls)):
            if (
                    (pygame.Rect(walls[i]).colliderect(
                pygame.Rect(self.pack_man_rect.topleft[0], self.pack_man_rect.topleft[1] - 3, 32, 3))
                and self.newspeed[1] < 0)
            or
                    (pygame.Rect(walls[i]).colliderect(
                pygame.Rect(self.pack_man_rect.bottomleft[0], self.pack_man_rect.bottomleft[1], 32, 3))
                and self.newspeed[1] > 0)
            or
                (pygame.Rect(walls[i]).colliderect(
                    pygame.Rect(self.pack_man_rect.topleft[0] - 3, self.pack_man_rect.topleft[1], 3, 32))
                and self.newspeed[0] < 0)
            or
                (pygame.Rect(walls[i]).colliderect(
                    pygame.Rect(self.pack_man_rect.topright[0] + 3, self.pack_man_rect.topright[1], 3, 32))
                and self.newspeed[0] > 0)
            ):
                if (self.speed == [0, 0]):
                    self.newspeed = [0, 0]
                return
        self.speed = self.newspeed
        self.newspeed = [0, 0]

    def collides_with_wall(self, rect):
        """Метод для обработки столкновений со стенами лабиринта"""
        if (self.pack_man_rect.colliderect(pygame.Rect(rect))):
            if (self.speed[0] > 0):
                self.pack_man_rect = self.pack_man_rect.move((
                    -abs(pygame.Rect(self.pack_man_rect).right - pygame.Rect(rect).x), 0
                ))
            elif (self.speed[0] < 0):
                self.pack_man_rect = self.pack_man_rect.move((
                    abs(pygame.Rect(self.pack_man_rect).x - pygame.Rect(rect).right), 0
                ))
            elif (self.speed[1] > 0):
                self.pack_man_rect = self.pack_man_rect.move((
                    0, -abs(pygame.Rect(self.pack_man_rect).bottom - pygame.Rect(rect).top)
                ))
            elif (self.speed[1] < 0):
                self.pack_man_rect = self.pack_man_rect.move((
                    0, abs(pygame.Rect(self.pack_man_rect).top - pygame.Rect(rect).bottom)
                ))
            self.speed = [0, 0]
            self.newspeed = [0, 0]
            return True
        return False

    def ghost_collision(self, ghosts):
        """Метод для обработки столкновений с привидиениями"""
        if (self.invincible == False):
            for i in range(len(ghosts)):
                if ((ghosts[i].ghost_rect.centerx - self.maxspeed
                     <= self.pack_man_rect.centerx <=
                     ghosts[i].ghost_rect.centerx + self.maxspeed) and
                    (ghosts[i].ghost_rect.centery - self.maxspeed
                     <= self.pack_man_rect.centery <=
                     ghosts[i].ghost_rect.centery + self.maxspeed) and
                ghosts[i].caught == False):
                    return True
                for j in range(len(ghosts[i].firearr)):
                    if ((ghosts[i].firearr[j].fire_rect.centerx - 5 <= self.pack_man_rect.centerx
                         <= ghosts[i].firearr[j].fire_rect.centerx + 5) and
                    (ghosts[i].firearr[j].fire_rect.centery - 5 <= self.pack_man_rect.centery
                     <= ghosts[i].firearr[j].fire_rect.centery + 5)):
                        return True
        else:
            for i in range(len(ghosts)):
                if (ghosts[i].fright == False):
                    ghosts[i].caught = False
                    ghosts[i].fright = True
                    ghosts[i].attack = False
                    ghostpos = [ghosts[i].ghost_rect.centerx, ghosts[i].ghost_rect.centery]
                    ghosts[i].ghost_img = pygame.image.load("resources/textures/run.png")
                    ghosts[i].ghost_img = pygame.transform.scale(ghosts[i].ghost_img, (32, 32))
                    ghosts[i].ghost_rect = pygame.Rect(0, 0, 32, 32)
                    ghosts[i].ghost_rect.center = (ghostpos[0], ghostpos[1])
                if ((ghosts[i].ghost_rect.centerx - self.maxspeed
                     <= self.pack_man_rect.centerx <=
                     ghosts[i].ghost_rect.centerx + self.maxspeed) and
                    (ghosts[i].ghost_rect.centery - self.maxspeed <=
                     self.pack_man_rect.centery <=
                     ghosts[i].ghost_rect.centery + self.maxspeed) and
                        ghosts[i].caught == False):
                    self.caughtghost += 1
                    self.score += 400 * self.caughtghost
                    ghosts[i].caught = True
                    ghostpos = [ghosts[i].ghost_rect.centerx, ghosts[i].ghost_rect.centery]
                    ghosts[i].ghost_img = pygame.image.load("resources/textures/caught.png")
                    ghosts[i].ghost_img = pygame.transform.scale(ghosts[i].ghost_img, (32, 32))
                    ghosts[i].ghost_rect = pygame.Rect(0, 0, 32, 32)
                    ghosts[i].ghost_rect.center = (ghostpos[0], ghostpos[1])

    def grain_collision(self, grains):
        """Метод проверяющий коллизию с зернами"""
        delelements = []
        for i in range(len(grains)):
            if self.pack_man_rect.colliderect(grains[i].grain_rect):
                delelements.append(i)
        delelements.sort(reverse=True)
        if (len(delelements) != 0 and self.invincible == False):
            pygame.mixer.music.load('resources/sounds/pacman_eat_grain.wav')
            pygame.mixer.music.set_volume(0.2)
            pygame.mixer.music.play()
        for i in range(len(delelements)):
            self.score += grains[delelements[i]].score
            if (grains[delelements[i]].type == "booster"):
                self.invincible = True
            del grains[delelements[i]]

    def check_portal(self):
        """Метод проверяющий коллизию порталов с порталом"""
        if (446 <= self.pack_man_rect.centerx <= 449 and
                283 <= self.pack_man_rect.centery <= 285):
            self.pack_man_rect.centerx = 55
            self.pack_man_rect.centery = 284
            self.speed = [self.maxspeed / 1.5, 0]
        elif (49 <= self.pack_man_rect.centerx <= 52 and
              283 <= self.pack_man_rect.centery <= 285):
            self.pack_man_rect.centerx = 445
            self.pack_man_rect.centery = 284
            self.speed = [-self.maxspeed / 1.5, 0] # пакмэн замедляется при прохождении через телепорт

    def pacman_movement(self, event, ghosts, menu, map, invisiblecounter):
        """Движение пакмэна"""
        self.shift(width, height)
        for i in range(len(map.walls)):
            if (self.collides_with_wall(map.walls[i])):
                break
        self.process_event(event, map.walls, menu.control)
        self.grain_collision(map.grains)
        if (self.invincible and invisiblecounter == 8):
            pygame.mixer.music.load('resources/sounds/ghost_fright.wav')
            pygame.mixer.music.set_volume(0.2)
            pygame.mixer.music.play()
            invisiblecounter = 0

        if (3 / self.maxspeed <= invisiblecounter <= 4 / self.maxspeed):
            for i in range(len(ghosts)):
                ghosts[i].readytoremovefright = True

        if (4 / self.maxspeed <= invisiblecounter < 8 / self.maxspeed): # сброс действия бустера
            for i in range(len(ghosts)):
                if (ghosts[i].fright and ghosts[i].caught == False):
                    ghosts[i].__init__(ghosts[i].color, ghosts[i].ghost_rect.centerx, ghosts[i].ghost_rect.centery)
            self.invincible = False
            for i in range(len(ghosts)):
                ghosts[i].fright = False
            if (self.score < 600):
                ghosts[1].attack = True
                ghosts[3].attack = True
            elif (self.score < 1500):
                ghosts[0].attack = True
                ghosts[2].attack = True
            else:
                for i in range(len(ghosts)):
                    ghosts[i].attack = True
            invisiblecounter = 8
        self.check_portal()
        return invisiblecounter

    def draw(self, screen):
        """Отрисовка пакмэна"""
        screen.blit(self.pack_man, self.pack_man_rect)
