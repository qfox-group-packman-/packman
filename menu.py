import pygame
import sys
import time

size = width, height = 500, 600
black = 0, 0, 0

class Button:
    def __init__(self, text, x, y, visible):
        self.font = pygame.font.Font('resources/menu/pacmanfont.ttf', 30)
        self.text = text
        self.x = x
        self.y = y
        self.visible = visible
        self.active = False
        self.color = (255, 255, 255)

    def Update(self, screen):
        """Отрисовка кнопки с текстом"""
        if self.visible:
            text = self.font.render(self.text, 0, self.color)
            screen.blit(text, (self.x, self.y))

class LifeIcon:
    def __init__(self, x, y):
        self.life_img = pygame.image.load('resources/map/lifeicon.png')
        self.life_img = pygame.transform.scale(self.life_img, (32, 32))
        self.life_rect = self.life_img.get_rect(center=(x, y))

    def draw(self, screen):
        """Отрисовка иконки"""
        screen.blit(self.life_img, self.life_rect)

class Menu:
    def __init__(self):
        self.mode = "default"  # or default, about, settings, play, game over
        self.start = False
        self.fps = 60 # or 30, 120
        self.score = 0
        self.control = "keys"  # or arrow
        self.difficulty = "normal" # or hard, hell, easy
        self.showfps = False
        self.cheats = False
        self.map_img = pygame.image.load('resources/menu/menu.png')
        self.map_img = pygame.transform.scale(self.map_img, (500, 600))
        self.map_rect = self.map_img.get_rect(center=((width // 2) + 6, height // 2))
        self.result = "You lost" # or "You win"
        self.level = 1 # or 2

        """Списов всех кнопок"""
        self.buttons = []
        self.buttons.append(Button("Play", 208, 160, True))
        self.buttons.append(Button("Settings", 170, 200, True))
        self.buttons.append(Button("About", 195, 240, True))
        self.buttons.append(Button("Сontrol: {}".format(self.control), 110, 160, False))
        self.buttons.append(Button("Back", 20, 550, False))
        self.buttons.append(Button("Max FPS: {}".format(self.fps), 110, 200, False))
        self.buttons.append(Button("Show FPS: {}".format(self.showfps), 110, 240, False))
        self.buttons.append(Button("Retry", 200, 550, False))
        self.buttons.append(Button("Difficulty: {}".format(self.difficulty), 110, 120, False))
        self.buttons.append(Button("Cheats: {}".format(self.cheats), 110, 280, False))

        pygame.mixer.music.load('resources/sounds/menu.wav')
        pygame.mixer.music.set_volume(0.3)
        pygame.mixer.music.play()


    def CheckButtons(self, mouseposx, mouseposy):
        """Проверка клика на кнопку"""
        for i in range(len(self.buttons)):
            if (pygame.Rect(self.buttons[i].x - len(self.buttons[i].text) // 2, self.buttons[i].y + 10,
                            len(self.buttons[i].text) * 20, 20).collidepoint(mouseposx, mouseposy) and
            self.buttons[i].visible):
                if (i == 0 or i == 7):
                    self.mode = "play"
                    self.start = True
                elif (i == 1):
                    self.mode = "settings"
                elif (i == 2):
                    self.mode = "about"
                elif (i == 3):
                    if (self.control == "keys"):
                        self.control = "arrows"
                    else:
                        self.control = "keys"
                elif (i == 4):
                    self.mode = "default"
                elif (i == 5):
                    if (self.fps == 30):
                        self.fps = 60
                    elif (self.fps == 60):
                        self.fps = 120
                    else:
                        self.fps = 30
                elif (i == 6):
                    if (self.showfps):
                        self.showfps = False
                    else:
                        self.showfps = True
                elif (i == 8):
                    if (self.difficulty == "normal"):
                        self.difficulty = "hard"
                    elif (self.difficulty == "hard"):
                        self.difficulty = "hell"
                    elif (self.difficulty == "hell"):
                        self.difficulty = "easy"
                    else:
                        self.difficulty = "normal"
                elif (i == 9):
                    if (self.cheats):
                        self.cheats = False
                    else:
                        self.cheats = True
                self.buttons[i].active = True

    def Update(self, screen):
        """Отрисовка меню и его разделов"""
        for i in range(len(self.buttons)):
            self.buttons[i].visible = False

        if (self.mode == "default"):
            screen.blit(self.map_img, self.map_rect)
            for i in range(3):
                self.buttons[i].visible = True
        elif (self.mode == "settings"):
            self.buttons[3].visible = True
            self.buttons[3].text = "Сontrol: {}".format(self.control)
            self.buttons[4].visible = True
            self.buttons[5].visible = True
            self.buttons[5].text = "Max FPS: {}".format(self.fps)
            self.buttons[6].visible = True
            self.buttons[6].text = "Show FPS: {}".format(self.showfps)
            self.buttons[8].visible = True
            self.buttons[8].text = "Difficulty: {}".format(self.difficulty)
            self.buttons[9].visible = True
            self.buttons[9].text = "Cheats: {}".format(self.cheats)
        elif (self.mode == "about"):
            font = pygame.font.Font('resources/menu/pacmanfont.ttf', 30)
            text = font.render("Created by QFox Group: ", 0, (255, 255, 255))
            screen.blit(text, (40, 100))
            text = font.render("Ivan Melnikov", 0, (255, 255, 255))
            screen.blit(text, (140, 140))
            text = font.render("Mathew Melnikov", 0, (255, 255, 255))
            screen.blit(text, (110, 180))
            self.buttons[4].visible = True
        elif (self.mode == "game over"):
            self.buttons[7].visible = True
            font = pygame.font.Font('resources/menu/pacmanfont.ttf', 30)
            text = font.render("Game over!", 0, (255, 255, 255))
            screen.blit(text, (150, 60))
            text = font.render(self.result, 0, (255, 255, 255))
            if (self.result == "You lost"):
                screen.blit(text, (165, 100))
            else:
                screen.blit(text, (175, 100))
            font2 = pygame.font.Font('resources/map/guifont.ttf', 35)
            text = font2.render("Your score: {}".format(self.score), 0, (255, 255, 255))
            screen.blit(text, (120, 180))
            f = open('resources/bestscore', 'r')
            self.bestscore = 0  # load from file
            for line in f:
                self.bestscore = int(line)
            f.close()
            text = font2.render("Best score: {}".format(self.bestscore), 0, (255, 255, 255))
            screen.blit(text, (120, 220))
            if (self.score > self.bestscore):
                f = open('resources/bestscore', 'w')
                f.write(str(self.score))
            self.buttons[4].visible = True

        for i in range(len(self.buttons)):
            if (self.buttons[i].visible == True):
                if (self.buttons[i].active != True):
                    self.buttons[i].Update(screen)
        for i in range(len(self.buttons)):
            if (self.buttons[i].visible):
                if (self.buttons[i].active == True):
                    self.buttons[i].color = (105, 105, 105)
                    self.buttons[i].Update(screen)
                    pygame.display.update()
                    time.sleep(0.1)
                    self.buttons[i].color = (255, 255, 255)
                    self.buttons[i].active = False