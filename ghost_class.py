import pygame
from game_object import GameObject
import random

width, height = 500, 600

black = (0, 0, 0)

class Fire(GameObject):
    def __init__(self, x, y):
        self.type = type
        self.score = 10
        self.fire_img = pygame.image.load('resources/map/fire.png')
        self.fire_img = pygame.transform.scale(self.fire_img, (11, 11))
        self.fire_rect = self.fire_img.get_rect(center=(x, y))
        self.counter = 0
    def update_texture(self, maxspeed):
        """Метод обновляющий текстуру огня"""
        if (0.9 / maxspeed <= self.counter <= 1.1 / maxspeed):
            pos = [self.fire_rect.centerx, self.fire_rect.centery]
            self.fire_img = pygame.image.load('resources/map/fire1.png')
            self.fire_img = pygame.transform.scale(self.fire_img, (11, 11))
            self.fire_rect = self.fire_img.get_rect(center=(pos[0], pos[1]))

class Ghost(GameObject):
    def __init__(self, color, x, y):
        super().__init__()
        self.speed = [0, 0]  # Скорость движения призрака по осям OX и OY
        self.color = color
        self.level = 1  # на каком уровне находиться игра
        self.attack = False  # Режим приведения, когда оно атакует пакмэна
        self.mainpoint = [0, 0]  # целевая точка призрака (куда он убегает при испуге)
        self.caught = False # переменная, изменяющаяся при поимке привидения
        self.firearr = [] # массив из огня, следующего за призраком
        self.counter = 0 # используется для регулировки следа из огня
        self.readytoremovefright = False
        self.run = False
        self.ghost_img = pygame.image.load(
            "resources/textures/ghosts/" + str(self.level) + "/" + self.color + "ghost.png").convert_alpha()
        self.fright = False # режим побега от пакмэна
        self.ghost_img = pygame.transform.scale(self.ghost_img, (32, 32))
        self.ghost_rect = pygame.Rect(0, 0, 32, 32)
        self.ghost_rect.center = (x, y)
        # Представление призрака в виде прямоугольника
        self.newspeed = [0, 0]
        self.dir = [0, 0] # направление движения призрака
        self.timer = 0 # используется для метода first_steps()
        self.animationcounter = 0
        self.maxspeed = 2  # устанавливает скорость  призрака

    def shift(self):
        """Метод приводящий призрака в движение"""
        self.animationcounter += 0.25
        if (self.animationcounter >= 2):
            self.animationcounter = 0
        self.update_texture()

        self.ghost_rect = self.ghost_rect.move(self.speed)
        if (self.speed == [0, 0] and self.timer <= 8):
            self.timer += 1

        if (self.speed[0] != [0, 0] and self.fright == False and self.caught == False):
            self.counter += 0.01

        if (self.counter >= 0.5 / self.maxspeed):
            self.counter = 0
            self.firearr.append(Fire(self.ghost_rect.centerx, self.ghost_rect.centery))

    def update_texture(self):
        """Метод отвечающий за анимацию призрака"""
        pos = [self.ghost_rect.centerx, self.ghost_rect.centery]

        if (self.caught == True and self.speed[1] == 0):
            self.ghost_img = pygame.image.load("resources/textures/caught.png").convert_alpha()
        elif (self.caught == True and self.speed[1] < 0):
            self.ghost_img = pygame.image.load("resources/textures/caughtup.png").convert_alpha()
        elif (self.caught == True and self.speed[1] > 0):
            self.ghost_img = pygame.image.load("resources/textures/caughtdown.png").convert_alpha()

        elif (0 <= self.animationcounter <= 1 and self.fright and self.readytoremovefright == False):
            self.ghost_img = pygame.image.load("resources/textures/run.png").convert_alpha()
        elif (self.fright and self.readytoremovefright == False):
            self.ghost_img = pygame.image.load("resources/textures/run1.png").convert_alpha()

        elif (0 <= self.animationcounter <= 1 and self.fright and self.readytoremovefright == True):
            self.ghost_img = pygame.image.load("resources/textures/run2.png").convert_alpha()
        elif (self.fright and self.readytoremovefright == True):
            self.ghost_img = pygame.image.load("resources/textures/run3.png").convert_alpha()

        elif (0 <= self.animationcounter <= 1 and self.speed[1] == 0):
            self.ghost_img = pygame.image.load(
                "resources/textures/ghosts/" + str(self.level) + "/" + self.color + "ghost.png").convert_alpha()
        elif (self.speed[1] == 0):
            self.ghost_img = pygame.image.load(
                "resources/textures/ghosts/" + str(self.level) + "/" + self.color + "ghost1.png").convert_alpha()

        elif (0 <= self.animationcounter <= 1 and self.speed[0] == 0):
            if (self.speed[1] < 0):
                self.ghost_img = pygame.image.load(
                    "resources/textures/ghosts/" + str(self.level) + "/" + self.color + "ghostup.png").convert_alpha()
            else:
                self.ghost_img = pygame.image.load(
                    "resources/textures/ghosts/" + str(self.level) + "/" + self.color + "ghostdown.png").convert_alpha()
        else:
            if (self.speed[1] < 0):
                self.ghost_img = pygame.image.load(
                    "resources/textures/ghosts/" + str(self.level) + "/" + self.color + "ghostup1.png").convert_alpha()
            else:
                self.ghost_img = pygame.image.load(
                    "resources/textures/ghosts/" + str(self.level) + "/" + self.color + "ghostdown1.png").convert_alpha()

        if (self.speed[0] < 0):
            self.ghost_img = pygame.transform.flip(self.ghost_img, True, False)

        self.ghost_img = pygame.transform.scale(self.ghost_img, (32, 32))
        self.ghost_rect = pygame.Rect(0, 0, 32, 32)
        self.ghost_rect.center = (pos[0], pos[1])

    def process_event(self, walls):
        """Метод обрабатывающий движение призрака"""
        self.first_steps()
        self.newspeed = [0, 0]
        if (self.speed != [0, 0] and self.counter >= 7):
            return
        if self.dir[0] != 0 or self.dir[1] != 0:
            if (self.dir[0] == -1 and self.speed[0] == 0):
                self.newspeed[0] = -self.maxspeed

            elif (self.dir[0] == 1 and self.speed[0] == 0):
                self.newspeed[0] = self.maxspeed

            elif (self.dir[1] == -1 and self.speed[1] == 0):
                self.newspeed[1] = -self.maxspeed

            elif (self.dir[1] == 1 and self.speed[1] == 0):
                self.newspeed[1] = self.maxspeed

        # заранее просчитываем коллизию
        if (self.newspeed[0] == 0 and self.newspeed[1] == 0 \
                or (self.newspeed[0] != 0 and self.newspeed[1] != 0)):
            return

        for i in range(len(walls)):
            if (
                    (pygame.Rect(walls[i]).colliderect(
                pygame.Rect(self.ghost_rect.topleft[0], self.ghost_rect.topleft[1] - 3, 32, 3))
                and self.newspeed[1] < 0)
            or
                    (pygame.Rect(walls[i]).colliderect(
                pygame.Rect(self.ghost_rect.bottomleft[0], self.ghost_rect.bottomleft[1], 32, 3))
                and self.newspeed[1] > 0)
            or
                (pygame.Rect(walls[i]).colliderect(
                    pygame.Rect(self.ghost_rect.topleft[0] - 3, self.ghost_rect.topleft[1], 3, 32))
                and self.newspeed[0] < 0)
            or
                (pygame.Rect(walls[i]).colliderect(
                    pygame.Rect(self.ghost_rect.topright[0] + 3, self.ghost_rect.topright[1], 3, 32))
                and self.newspeed[0] > 0)
            ):
                if (self.speed == [0, 0]):
                    self.dir = [0, 0]
                return

        self.speed = self.newspeed
        self.newspeed = [0, 0]

    def collides_with_wall(self, rect):
        """Функция для обработки столкновений со стенами лабиринта"""
        if (self.ghost_rect.colliderect(pygame.Rect(rect))):
            if (self.speed[0] > 0):
                self.ghost_rect = self.ghost_rect.move((
                    -abs(pygame.Rect(self.ghost_rect).right - pygame.Rect(rect).x), 0
                ))
            elif (self.speed[0] < 0):
                self.ghost_rect = self.ghost_rect.move((
                    abs(pygame.Rect(self.ghost_rect).x - pygame.Rect(rect).right), 0
                ))
            elif (self.speed[1] > 0):
                self.ghost_rect = self.ghost_rect.move((
                    0, -abs(pygame.Rect(self.ghost_rect).bottom - pygame.Rect(rect).top)
                ))
            elif (self.speed[1] < 0):
                self.ghost_rect = self.ghost_rect.move((
                    0, abs(pygame.Rect(self.ghost_rect).top - pygame.Rect(rect).bottom)
                ))
            self.speed = [0, 0]
            return True
        return False

    def first_steps(self):
        """Метод обеспечивающий выход призраков из комнаты в нужном порядке"""
        if (self.timer > 6):
            return
        self.dir = [0, 0]
        if (self.timer == 0):
            self.speed = [0, 0]
        if (self.color == "red"):
            #self.speed = [0, 0]
            self.dir[0] = 1
            self.timer += 1
        elif (self.timer == 1):
            self.dir[1] = -1
            if (self.color == "pink"):
                self.dir[1] = -1
        elif (self.color == "pink"):
            self.dir[0] = -1
            self.timer = 6
        elif (self.color == "blue" and self.timer == 2):
            self.dir = [0, 0]
            self.dir[0] = 1
        elif (self.color == "blue" and self.timer == 3):
            self.dir = [0, 0]
            self.dir[0] = 1
            self.timer = 6
        elif (self.color == "orange"  and self.timer == 3):
            self.dir = [0, 0]
            self.dir[0] = -1
        elif (self.color == "orange" and self.timer == 4):
            self.dir = [0, 0]
            self.dir[0] = -1
            self.timer = 6
        if (self.color == "orange" and 230 <= self.ghost_rect.x <= 236 and self.timer == 3  and self.ghost_rect.y == 259):
            self.ghost_rect.x = 233
            self.dir = [0, 0]
            self.dir[1] = -1
        if (self.color == "blue" and 230 <= self.ghost_rect.x <= 236 and self.timer == 2 and self.ghost_rect.y == 259):
            self.ghost_rect.x = 233
            self.dir = [0, 0]
            self.dir[1] = -1

    def check_dir(self, walls):
        """Метод позволяющий узнать свободные строны относительно призрака"""
        """(слева, справа, сверху, снизу). 1 - свободно, 0 - занято"""
        newdir = [1, 1, 1, 1]
        for i in range(len(walls)):
            if (pygame.Rect(walls[i]).colliderect(
                pygame.Rect(self.ghost_rect.topleft[0], self.ghost_rect.topleft[1] - 3, 32, 3))):
                newdir[2] = 0
            elif (pygame.Rect(walls[i]).colliderect(
                pygame.Rect(self.ghost_rect.bottomleft[0], self.ghost_rect.bottomleft[1], 32, 3))):
                newdir[3] = 0
            elif (pygame.Rect(walls[i]).colliderect(
                    pygame.Rect(self.ghost_rect.topleft[0] - 3, self.ghost_rect.topleft[1], 3, 32))):
                newdir[0] = 0
            elif (pygame.Rect(walls[i]).colliderect(
                    pygame.Rect(self.ghost_rect.topright[0] + 3, self.ghost_rect.topright[1], 3, 32))):
                newdir[1] = 0
        return newdir

    def removestick(self, newdir):
        """Метод выводящий призрака из тупика"""
        self.dir = [0, 0]
        num = 1
        for i in range(4):
            if (newdir[i]):
                num += 1
        rand = random.randint(0 , num - 1)
        newdir = [0, 0, 0, 0]
        newdir[rand] = 1
        if (newdir[0] == 1):
            self.dir[0] = -1
        elif (newdir[1] == 1):
            self.dir[0] = 1
        elif (newdir[2] == 1):
            self.dir[1] = -1
        elif (newdir[3] == 1):
            self.dir[1] = 1

    def decision(self, pacmanx, pacmany, pacmandir, walls):
        """Метод для принятия решений привидениями"""
        self.dir = [0, 0]
        newdir = self.check_dir(walls)
        #######################
        # установка цели
        purpose = [pacmanx, pacmany]
        if (self.caught):
            if (249 <= self.ghost_rect.centerx <= 251 and
           235 <= self.ghost_rect.centery <= 237):
                self.speed = [0, 0]
                self.dir = [0, 0]
                self.caught = 0
                self.attack = True
                self.fright = False
                self.__init__(self.color, 250, 236)
            else:
                if ((34 <= self.ghost_rect.centerx <= 147 and
                415 <= self.ghost_rect.centery <= 492) or
                (353 <= self.ghost_rect.centerx <= 465 and
                415 <= self.ghost_rect.centery <= 492)
                ):
                    purpose = [250, 523]
                else:
                    purpose = [250, 236]
        elif (self.attack == False or self.fright): # если пакмэн съел бустер и приближается к привидению,
            # то призрак пытается убежать
            if (((abs(self.ghost_rect.x - pacmanx) ** 2) + (abs(self.ghost_rect.y - pacmany) ** 2)) ** 0.5 <= 160):
                if (abs(self.ghost_rect.x - pacmanx) < abs(self.ghost_rect.y - pacmany)):
                    if (pacmanx >= self.ghost_rect.centerx and newdir[0] != 0):
                        purpose[0] -= 192
                    elif (pacmanx < self.ghost_rect.centerx and newdir[1] != 0):
                        purpose[0] += 192
                else:
                    if (pacmany <= self.ghost_rect.centery and newdir[3] != 0):
                        purpose[1] += 192
                    elif (pacmany > self.ghost_rect.centery and newdir[2] != 0):
                        purpose[1] -= 192
                    elif (pacmanx >= self.ghost_rect.centerx and newdir[0] != 0):
                        purpose[0] -= 192
                    elif (pacmanx < self.ghost_rect.centerx and newdir[1] != 0):
                        purpose[0] += 192
            else: # если пакмэн далеко, то призрак бежит на свою целевую точку
                purpose = self.mainpoint
        elif (self.color == "pink"):
            if (pacmandir[0] > 0):
                purpose[0] += 128
            elif (pacmandir[0] < 0):
                purpose[0] -= 128
            elif (pacmandir[1] > 0):
                purpose[1] += 128
            elif (pacmandir[1] < 0):
                purpose[1] -= 128
        elif (self.color == "blue"):
            if (pacmandir[1] > 0):
                purpose[1] -= 128
                purpose[0] -= 64
            elif (pacmandir[1] < 0):
                purpose[1] += 128
                purpose[0] += 64
            elif (pacmandir[0] > 0):
                purpose[1] -= 64
                purpose[0] -= 128
            elif (pacmandir[0] < 0):
                purpose[1] += 64
                purpose[0] += 128
        elif (self.color == "orange"):
            if (((abs(self.ghost_rect.x - pacmanx) + abs(self.ghost_rect.y - pacmany)) ** 0.5) * 32 < 256):
                purpose = self.mainpoint
        #######################
        # движение к цели
        if (abs(self.ghost_rect.x - purpose[0]) > abs(self.ghost_rect.y - purpose[1]) and
                newdir[0] and self.ghost_rect.x > purpose[0]):
            self.dir[0] = -1
        elif (abs(self.ghost_rect.x - purpose[0]) > abs(self.ghost_rect.y - purpose[1]) and
                newdir[1] and self.ghost_rect.x < purpose[0]):
            self.dir[0] = 1
        elif (newdir[2] and self.ghost_rect.y > purpose[1]):
            self.dir[1] = -1
        elif (newdir[3] and self.ghost_rect.y < purpose[1]):
            self.dir[1] = 1
        else:
            self.removestick(newdir)

    def ghosts_movement(self, pacman, map):
        """Движение призрака"""
        self.shift()
        for i in range(len(map.walls)):
            if (self.collides_with_wall(map.walls[i])):
                break
        self.decision(pacman.pack_man_rect.centerx, pacman.pack_man_rect.centery, pacman.speed, map.walls)
        self.process_event(map.walls)
        delelements = []
        for j in range(len(self.firearr)):
            self.firearr[j].counter += 0.01
            self.firearr[j].update_texture(self.maxspeed)
            if (self.firearr[j].counter >= 2 / self.maxspeed):
                delelements.append(j)
        for j in range(len(delelements)):
            del self.firearr[delelements[j]]

    def draw(self, screen):
        """Отрисовка призрака и огненного следа"""
        for j in range(len(self.firearr)):
            screen.blit(self.firearr[j].fire_img, self.firearr[j].fire_rect)
        screen.blit(self.ghost_img, self.ghost_rect)
