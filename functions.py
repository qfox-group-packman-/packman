import pygame

def reset_all(ghosts, pacman, emptyroom, map, size):
    """Функция для сброса"""
    ghosts[0].ghost_rect.centerx = 250
    ghosts[0].ghost_rect.centery = 236
    ghosts[1].ghost_rect.centerx = 250
    ghosts[1].ghost_rect.centery = 283
    ghosts[2].ghost_rect.centerx = 218
    ghosts[2].ghost_rect.centery = 283
    ghosts[3].ghost_rect.centerx = 282
    ghosts[3].ghost_rect.centery = 283
    for i in range(len(ghosts)):
        ghosts[i].__init__(ghosts[i].color, ghosts[i].ghost_rect.centerx, ghosts[i].ghost_rect.centery)
    if (emptyroom):
        del map.walls[len(map.walls) - 1]
        emptyroom = False
    ghosts[0].attack = False
    ghosts[1].attack = True
    ghosts[2].attack = False
    ghosts[3].attack = True
    pacman.__init__()
    pacman.pack_man_rect.centerx = size[0] // 2
    pacman.pack_man_rect.centery = size[1] // 2 + 32
    pacman.invincible = False

    pygame.mixer.music.load('resources/sounds/pacman_appears_after_death.wav')
    pygame.mixer.music.set_volume(0.2)
    pygame.mixer.music.play()

def set_difficulty(menu, ghosts, pacman):
    """Функция выставляющая сложность игры"""
    for i in range(len(ghosts)):
        if (menu.difficulty == "normal"):
            ghosts[i].maxspeed = 2
        elif (menu.difficulty == "hard"):
            ghosts[i].maxspeed = 3
        elif (menu.difficulty == "hell"):
            ghosts[i].maxspeed = 3.5
        else:
            ghosts[i].maxspeed = 1.5
        if (menu.level == 2):
            ghosts[i].level = 2
            ghosts[i].maxspeed *= 1.1
    if (menu.level == 1):
        pacman.maxspeed = ghosts[0].maxspeed
    else:
        pacman.maxspeed = ghosts[0].maxspeed / 1.1

def check_score(pacman, ghosts):
    """Функция которая регулирует количество призраков вовлеченных в погоню"""
    if (600 <= pacman.score < 1500):  # при достижении 600 очков, другие призраки начинают гоняться за пакмэном
        for i in range(len(ghosts)):
            if (ghosts[i].attack == False):
                ghosts[i].attack = True
            else:
                ghosts[i].attack = False
    elif (pacman.score >= 1500):  # при достижении 1500 очков играть начинаю все призраки
        for i in range(len(ghosts)):
            ghosts[i].attack = True

def draw_all(screen, menu, map, pacman, ghosts, lifeicons, clock, pause):
    """Отрисовка всего, что находится на карте"""
    if (menu.mode == "play"):  # отрисовка всех объектов на карте, которая происходит и во время паузы
        font = pygame.font.Font('resources/map/guifont.ttf', 30)
        pausetext = font.render("PAUSE", 0, (255, 255, 255))
        map.update(screen)
        pacman.draw(screen)
        for i in range(len(ghosts)):
            ghosts[i].draw(screen)
        font = pygame.font.Font('resources/map/guifont.ttf', 35)
        scoretext = font.render(str(pacman.score), 0, (255, 255, 255))
        screen.blit(scoretext, (410, 10))
        leveltext = font.render("Level: {}".format(menu.level), 0, (255, 255, 255))
        screen.blit(leveltext, (195, 10))

        for i in range(len(lifeicons)):
            lifeicons[i].draw(screen)

        if (menu.showfps):
            text1 = "FPS: {:0.0f}".format(clock.get_fps())
            text = font.render(text1, 0, (255, 255, 255))
            screen.blit(text, (21, 10))

        if (pause):
            screen.blit(pausetext, (215, 553))

def cheats(event, menu, map, pause, time):
    if event.type == pygame.KEYDOWN and menu.cheats:
        if event.key == pygame.K_c:
            map.grains.clear()
            time.sleep(0.2)